﻿open System
open System.Reflection
open System.IO
open CsvHelper
open LinqToExcel
open System.Globalization
open System.Security
open System.Text.RegularExpressions

let writeFileStream streamAction (fullFilePath:string) =
  use stream = new StreamWriter(File.Open(fullFilePath, FileMode.Create))
  streamAction stream

let readExcelFile processWorksheet (filename:string) =
  let validDateFormats = 
    [|"d/M/yyyy h:mm:ss tt"; "d/M/yyyy h:mm tt";
     "dd/MM/yyyy hh:mm:ss"; "d/M/yyyy h:mm:ss";
     "d/M/yyyy hh:mm tt"; "d/M/yyyy hh tt";
     "d/M/yyyy h:mm"; "d/M/yyyy h:mm";
     "dd/MM/yyyy"; "d/M/yyyy"; "dd/M/yyyy"; "d/MM/yyyy";
     "dd/MM/yyyy hh:mm"; "dd/M/yyyy hh:mm"|]

  let parseDates (validFormats:string[]) (outputFormat:string) (value:Object) =
    match value with
    | :? DateTime as datecell -> datecell.ToString(outputFormat) 
    | v -> 
      match DateTime.TryParseExact(v.ToString(), validFormats, new CultureInfo("en-GB"), DateTimeStyles.None) with
      | true, d -> d.ToString(outputFormat)
      | _ -> v.ToString()

  let parseCell (cell:Cell) = parseDates validDateFormats "yyyy-MM-dd" cell.Value

  let getRecords (ws:Query.ExcelQueryable<RowNoHeader>) =
    ws |> Seq.map (fun row -> row |> Seq.map parseCell )
  
  use excel = new ExcelQueryFactory(filename)
  for worksheetName in excel.GetWorksheetNames() do
    let ws = excel.WorksheetNoHeader(worksheetName)
    processWorksheet worksheetName (getRecords ws)

let writeCsv logrow values (stream:TextWriter) =
  use csv = new CsvWriter(stream)
  let mutable idx = 0
  for row in values do
    logrow idx
    idx <- idx + 1
    for v in row do
      csv.WriteField(v)
    csv.NextRecord()

let printExcelToCsv outputFolder (filename:string) =
  let formatFullFilePath name = Path.Combine(outputFolder, name + ".csv")
  let logrow rownum = if rownum % 10 = 0 then printf "."
  let logcsvfile name = printf "\nWRITING - %s: " name; name
  let writeCsv worksheetName vals = formatFullFilePath worksheetName |> logcsvfile |> writeFileStream (writeCsv logrow vals)
  readExcelFile writeCsv filename

type InputOptions = {
  Help:bool
  InputPath:string
  OutputPath:string
  RegexFilter:string
  Error:string
  TestOnly:bool
}

let parseArgs (argv:string[]) (commands:(string [] * int * (InputOptions * string[] -> InputOptions)) list) help defaultOptions = 
  let mutable options = defaultOptions
  let rec setCommand (args:string[]) = 
    if (args.Length <> 0) then
      let command, count, setter =
        match commands |> List.where (fun (c,_,_) -> Array.contains (args.[0].ToUpper()) c ) with
        | [] -> ([||], 0, help)
        | v -> v.[0]
      if (count > args.Length - 1) then 
        options <- help (options, [||])
        options <- {options with Error="\nERROR: The command: " + (command |> String.concat " ") + " requires " + string(count) + " arguments.\n" }
      else
        options <- setter (options, args.[1..count])
        setCommand args.[count+1..]

  setCommand argv
  options

type Success<'T> =
  | Success of 'T
  | Error of string
let filterSuccess vals =
  seq { for vl in vals do match vl with | Success v -> yield v; | Error _ -> () }

let rec recurseFolders path = //TODO AsyncSeq
  let getLegitPath p =
    try
      let di = DirectoryInfo(p)
      let childFolders = Directory.GetDirectories(di.FullName)
      match di.Exists with
      | true -> Success (di.FullName, childFolders) 
      | _ -> Error ("Can't find folder: " + di.FullName)
    with
    | :? ArgumentNullException | :? ArgumentException -> Error "Bad path"
    | :? SecurityException -> Error "Permission denied"
    | :? PathTooLongException -> Error "Path is too long for windows - sorry :("
    | :? DirectoryNotFoundException -> Error "Can't find directory"
    | ex -> Error ex.Message
  seq {
    let legitpath = getLegitPath path
    match legitpath with
    | Error msg -> yield Error msg
    | Success (p, children) -> 
      yield Success p
      for f in children do
        yield! recurseFolders f
  }
let getFiles folder =
  try
    Success (Directory.GetFiles(folder) |> Array.map (fun full -> (Path.GetDirectoryName(full), Path.GetFileName(full)) ))
  with
  | ex -> Error ex.Message
let filterFiles filter (_,file) = Regex.IsMatch(file, filter, RegexOptions.IgnoreCase)
let filterExcelFiles (_, file:string) =
  match file.LastIndexOf('.') with
  | -1 -> false
  | v -> 
    let ext = file.Substring(v).ToUpper()
    ext = ".XLS" || ext = ".XLSX" || ext = ".XLSM"
let createFolder (folders) =
  try
    Success (Directory.CreateDirectory(Path.Combine(folders)).FullName)
  with
  | ex -> Error ex.Message

[<EntryPoint>]
let main argv =
  let commands =
    [ ("Prints this help menu.                                  "           , [|"-?"; "-H"; "HELP" |], 0, (fun (o, _) -> { o with Help=true }))
      ("Test the regex filter. Will not process files.          "           , [|"-T"; "TEST"  |]     , 0, (fun (o, _) -> { o with TestOnly=true }))
      ("Apply a regex filter to files - not folders.            "           , [|"-R"; "REGEX" |]     , 1, (fun (o, (v:string[])) -> { o with RegexFilter=v.[0] }))
      ("Input directory to start searching - defaults to current directory.", [|"-I"; "IN" |]        , 1, (fun (o, v) -> { o with InputPath=v.[0].Trim('"') }))
      ("Output directory - defaults to input-dir/extract-filename."         , [|"-O"; "OUT" |]       , 1, (fun (o, v) -> { o with OutputPath=v.[0].Trim('"') }) )]
  let cwd =  Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
  let defaultOptions = { 
    Help=false; 
    InputPath=cwd;
    OutputPath="";
    RegexFilter="";
    TestOnly=false; 
    Error="" }
  let postParserDefaults options =
    if String.IsNullOrWhiteSpace options.OutputPath then { options with OutputPath=options.InputPath } else options
  let (_,_,_,help) = commands.[0]
  let options = parseArgs argv (commands |> List.map (fun (_,a,b,c) -> (a,b,c))) help defaultOptions |> postParserDefaults
  let printHelp() = 
    if not (String.IsNullOrWhiteSpace options.Error) then
      printfn "%s" options.Error
    printfn "### Excel Sheets to CSV ###"
    printfn "All options below are optional\n"
    for (description,c,_,_) in commands do
      printfn "%s %s" ((c |> String.concat " ").PadRight(20, '.')) description
    printfn "\n\n\n"

  if (options.Help || options.Error.Length > 0) 
  then printHelp()
  else 
    let files = recurseFolders options.InputPath 
              |> filterSuccess 
              |> Seq.map getFiles
              |> filterSuccess
              |> Seq.concat
              |> Seq.where filterExcelFiles
              |> Seq.where (filterFiles options.RegexFilter)

    if (options.TestOnly) then //If TESTING the regex expression, just show the included files
      printfn "The following files will be processed:"
      for (folder, file) in files do
        printfn "%s\\%s" folder file
    else if files |> Seq.exists (fun _ -> true) then //Proccess Excel files, if there are any
      try
      for (folder, file) in files do
        printf "Processing: %s\\%s" folder file
        match createFolder [|options.OutputPath; ("extract-" + file) |] with
        | Success v -> printExcelToCsv v (folder + "\\" + file)
        | Error _ -> ()
      with
      | ex when ex :? InvalidOperationException && ex.Message.Contains("ACE.OLEDB") -> printfn "\nERROR: %s\n\nRESOLUTION: You will need to download a driver (similar to that in the link below) from microsoft to use this program.\nhttps://www.microsoft.com/en-us/download/details.aspx?id=23734" ex.Message
      | ex -> printfn "\nERROR: %s" ex.Message
    else if argv.Length = 0 then //If there are no Excel files in the current folder and no inputs from the user, show the help
      printHelp()
  0 
