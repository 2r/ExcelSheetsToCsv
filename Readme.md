# Excel Sheets to CSV

A quick and dirty project to spit out CSV files from Excel spreadsheets.

The project uses the Fody Costura weaver to bundle them into a single assembly, making the resulting exe portable. The only rquirement for the program to run is then the OLEDB provider used by LinqToExcel. 

## Running 

- Type the name of the exe, e.g. `ExcelSheetsToCsv` and provide input arguments. 
- For help type `-?`. 
- Add the relevant PATH to your system evironment variables if you want to access it from anywhere. 